import configparser
import csv
import os
import re
import cv2
import matplotlib.pyplot as plt
import numpy as np
import scipy.ndimage as filters
import xlsxwriter
from PIL import Image, ImageDraw
from matplotlib.colors import LinearSegmentedColormap
import matplotlib
matplotlib.use('agg')


DATA = []
CONFIG = configparser.ConfigParser()


def init_dir(config: list, dir: os.path) -> bool:
    CWD = os.getcwd()
    dir_path = os.path.join(CWD, dir)

    if os.path.exists(dir_path):
        print(f"[+] Path '{dir_path}' already exists.")
        return dir_path
    else:
        try:
            os.makedirs(dir_path, exist_ok=True)
            print(f"[+] Path '{dir_path}' has been successfully created.")
            return dir_path
        except Exception as e:
            print(e)
    return None


def check_config(config: list, height=None, width=None):
    if config is None or len(config) == 0:
        return False
    sections = config.sections()
    if "PATH" in sections and "FILE" in sections and "LAB" in sections and "MAP" in sections and "HEATMAP" in sections and "PROGRAM" in sections:
        height, width = set_imagedata(os.path.join(os.getcwd(), config["PATH"]["src_dir"], config["PATH"]["img_dir"],
                                                   config["FILE"]["map_filename"]))

        if not height == 0 and not width == 0:
            config["MAP"] = {"is_valid": "true",
                             "width": width, "height": height}
        else:
            config["MAP"] = {"is_valid": "false", "width": 0, "height": 0}

        with open('config.ini', 'w+') as configfile:
            config.write(configfile)
        return True
    else:
        config["PROGRAM"] = {
            "save_plot_figures": "false", "save_heatmaps": "true"}
        config["HEATMAP"] = {"type": "jet",
                             "colorbar": "true", "sigma": "40", "overlay": "0.6"}
        config["PATH"] = {"src_dir": "src", "data_dir": "data", "img_dir": "img", "output_dir": "output",
                          "heatmap_dir": "heatmap", "fig_dir": "fig", "overlay_dir": "overlay"}
        config["FILE"] = {
            "output_filename": "04_wifi_meres.xlsx", "map_filename": "map.jpg"}
        config["LAB"] = {"minimum_measurements": 4}
        height, width = set_imagedata(os.path.join(os.getcwd(), config["PATH"]["src_dir"], config["PATH"]["img_dir"],
                                                   config["FILE"]["map_filename"]))
        if not height == 0 and not width == 0:
            config["MAP"] = {"is_valid": "True",
                             "width": width, "height": height}
            with open('config.ini', 'w+') as configfile:
                config.write(configfile)
                print("[*] Config file has been successfully created.")
            return True
        else:
            config["MAP"] = {"is_valid": "false", "width": 0, "height": 0}
            with open('config.ini', 'w+') as configfile:
                config.write(configfile)
                print("[*] Config file has been successfully created.")
            return True
    return False


def set_imagedata(map_file):
    if not os.path.exists(map_file):
        print(f"[!] src/data/[mapfile] does not exists.")
        return (0, 0)
    try:
        image = cv2.imread(map_file)
    except Exception as e:
        return (0, 0)
    height, width, _ = image.shape
    return (int(height), int(width))


def init(config: configparser):
    try:
        config.read('config.ini')

    except Exception as e:
        print("[!] Error during loading config file. Exiting...")
        exit(0)
    if not check_config(config):
        print("[!] Error during loading config file. Exiting...")
        exit(0)
    init_dir(config, os.path.join(
        config["PATH"]["src_dir"], config["PATH"]["data_dir"]))
    init_dir(config, os.path.join(
        config["PATH"]["src_dir"], config["PATH"]["img_dir"]))
    init_dir(config, os.path.join(
        config["PATH"]["src_dir"], config["PATH"]["overlay_dir"]))
    init_dir(config, os.path.join(
        config["PATH"]["output_dir"], config["PATH"]["heatmap_dir"]))
    init_dir(config, os.path.join(
        config["PATH"]["output_dir"], config["PATH"]["fig_dir"]))


def check_number_of_files(config: list, filelist: list = None):
    if filelist is None:
        filelist = []
    number_of_files = len(filelist)
    if number_of_files == 0:
        print("[!] No files in the 'data' directory. Exiting...")
        exit(0)
    elif number_of_files < int(config["LAB"]["minimum_measurements"]):
        print(
            f"[*] Number of files should be at least {int(config['LAB']['minimum_measurements'])}. Found: {number_of_files}")


def read_data(config: list, file_idx: int, filename: str, data: list = None):
    ssid_regex = r"[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-(\S{1,}).csv"
    mac_regex = r"-([a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2})"
    ssid = ""
    mac_addr = ""
    path = os.path.join(
        os.getcwd(), config["PATH"]["src_dir"], config["PATH"]["data_dir"], filename)

    if re.search(ssid_regex, filename) is not None:
        ssid = re.search(ssid_regex, filename).group(1)

    if re.search(mac_regex, filename) is not None:
        mac_addr = re.search(mac_regex, filename).group(1)

    measureddata = []
    labeldata = []
    if config["MAP"].getboolean("is_valid") and int(config["MAP"]["width"]) != 0 and int(config["MAP"]["height"]) != 0:
        width = int(config["MAP"]["width"])
        height = int(config["MAP"]["height"])
        imagedata = np.zeros(height * width)
        imagedata = imagedata.reshape((height, width))
        for x in range(0, width):
            for y in range(0, height):
                imagedata[y][x] = -80

        with open(path, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=';')
            next(reader)
            for idx, row in enumerate(reader):
                x = float(row[1])
                y = float(row[2])
                imagedata[round(y)][round(x)] = int(row[0])
                measureddata.append(int(row[0]))
                labeldata.append({"x": x, "y": y, "label": idx + 1})
        data.append({"ssid": ssid, "mac": mac_addr, "measureddata": measureddata, "imagedata": imagedata,
                     "labeldata": labeldata})
    else:
        with open(path, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=';')
            next(reader)
            for row in reader:
                measureddata.append(int(row[0]))
        data.append({"ssid": ssid, "mac": mac_addr,
                    "measureddata": measureddata})
    print(
        f"[+] Data in '{filename}' has been successfully appended to output file")


def merge_data_to_spreadsheet(config, data):
    path = os.path.join(
        os.getcwd(), config["PATH"]["output_dir"], config["FILE"]["output_filename"])
    workbook = xlsxwriter.Workbook(path)
    worksheet = workbook.add_worksheet()
    worksheet.write("A1", "SSID")
    worksheet.write("A2", "MAC-address")
    for data_idx, data_obj in enumerate(data):
        worksheet.write(chr(ord('A') + data_idx + 1) + '1', data_obj["ssid"])
        worksheet.write(chr(ord('A') + data_idx + 1) + '2', data_obj["mac"])

        for row_idx, row in enumerate(data_obj["measureddata"]):
            worksheet.write(chr(ord('A')) + str(row_idx + 3), row_idx + 1)
            worksheet.write(chr(ord('A') + data_idx + 1) +
                            str(row_idx + 3), row)

    workbook.close()


def main(data: list = None, config: list = None):
    if data is None:
        data = []
    if config is None:
        config = []

    # Initialization
    init(config)
    DATA_DIR_PATH = os.path.join(
        os.getcwd(), config["PATH"]["src_dir"], config["PATH"]["data_dir"])
    output_file = os.path.join(
        os.getcwd(), config["PATH"]["output_dir"], config["FILE"]["output_filename"])

    # Input data files
    filelist = [x.split('.')[0] for x in os.listdir(
        DATA_DIR_PATH) if x.split('.')[1] == "csv"]
    check_number_of_files(config, filelist)

    # Reading in data and creating the spreadsheet
    for file_idx, filename in enumerate(filelist):
        read_data(config, file_idx, f"{filename}.csv", data)
        merge_data_to_spreadsheet(config, data)
    print(f"[+] Succesfully created .xlsx file '{output_file}'")


def save_plot_figures(data, config):
    if not config["PROGRAM"].getboolean("save_plot_figures"):
        return
    if not config["MAP"].getboolean("is_valid") or int(config["MAP"]["width"]) == 0 or int(
            config["MAP"]["height"]) == 0:
        return
    else:
        colors = [(0, 0, 1), (0, 1, 1), (0, 1, 0.75), (0, 1, 0), (0.75, 1, 0), (1, 1, 0), (1, 0.8, 0), (1, 0.7, 0),
                  (1, 0, 0)]
        cm = LinearSegmentedColormap.from_list('sample', colors)
        for data_idx, data_obj in enumerate(data):
            imagedata = filters.gaussian_filter(
                data_obj["imagedata"], sigma=int(config["HEATMAP"]["sigma"]))
            plt.title(f'{data_obj["ssid"]}-{data_obj["mac"]}.jpg')
            plot_path = os.path.join(os.getcwd(), config["PATH"]["output_dir"], config["PATH"]["fig_dir"],
                                     f'{data_obj["ssid"]}_{data_obj["mac"]}.jpg')
            for label in data_obj["labeldata"]:
                plt.plot(label["x"], label["y"], "ob")
                plt.text(label["x"] * (1 + 0.03), label["y"] *
                         (1 + 0.03), label["label"], fontsize=8)

            if config["HEATMAP"]["type"] == "jet":
                plt.imshow(imagedata, cmap='jet', interpolation='nearest')
            else:
                plt.imshow(imagedata, cmap=cm, interpolation='nearest')

            if config["HEATMAP"].getboolean("colorbar"):
                plt.colorbar()

            plt.savefig(plot_path)
            plt.close()
            print(f"[+] Succesfully created plot_figure '{plot_path}")


def plot_points(coords, labels, filename, background_filename):
    # Load the background image
    background = Image.open(background_filename)

    # Create the scatter plot
    fig, ax = plt.subplots()

    # Add the labels
    draw = ImageDraw.Draw(background)
    for i, label in enumerate(labels):
        x, y = coords[i]
        draw.ellipse((x - 4, y - 4, x + 4, y + 4), fill='blue')
        draw.text((x + 5, y + 5), str(labels[i]),
                  fill='black', align='left', font=None)

    # Hide the x and y axis
    ax.axis('off')

    # Save the plot as a PNG file
    plt.draw()
    image = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)
    image = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))
    plt.imsave(filename, image)

    # Open the saved plot and resize it to match the background image
    plot_image = Image.open(filename)
    plot_image = plot_image.resize(background.size)

    # Convert the plot image to RGBA mode and remove the white background
    plot_image = plot_image.convert('RGBA')
    datas = plot_image.getdata()
    newData = []
    for item in datas:
        if item[0] == 255 and item[1] == 255 and item[2] == 255:
            newData.append((255, 255, 255, 0))
        else:
            newData.append(item)
    plot_image.putdata(newData)

    # Overlay the plot image onto the background image
    overlaid_image = Image.alpha_composite(
        background.convert('RGBA'), plot_image)

    # Save the overlaid image
    overlaid_image.save(filename)


def save_heatmap_images(data, config):
    if not config["PROGRAM"].getboolean("save_heatmaps"):
        return

    if not config["MAP"].getboolean("is_valid") or int(config["MAP"]["width"]) == 0 or int(
            config["MAP"]["height"]) == 0:
        return

    colors = [(0, 0, 1), (0, 1, 1), (0, 1, 0.75), (0, 1, 0), (0.75, 1, 0), (1, 1, 0), (1, 0.8, 0), (1, 0.7, 0),
              (1, 0, 0)]
    cm = LinearSegmentedColormap.from_list('sample', colors)
    background = Image.open(
        os.path.join(os.getcwd(), config["PATH"]["src_dir"], config["PATH"]["img_dir"], config["FILE"]["map_filename"]))
    background = background.convert("RGBA")

    for data_idx, data_obj in enumerate(data):
        imagedata = filters.gaussian_filter(
            data_obj["imagedata"], sigma=int(config["HEATMAP"]["sigma"]))
        plt.title(f'{data_obj["ssid"]}-{data_obj["mac"]}.jpg')

        overlay_path = os.path.join(os.getcwd(), config["PATH"]["src_dir"], config["PATH"]["overlay_dir"],
                                    f'overlay_{data_obj["ssid"]}_{data_obj["mac"]}.jpg')
        heatmap_path = os.path.join(os.getcwd(), config["PATH"]["output_dir"], config["PATH"]["heatmap_dir"],
                                    f'heatmap_{data_obj["ssid"]}_{data_obj["mac"]}.png')

        if config["HEATMAP"]["type"] == "jet":
            plt.imsave(overlay_path, imagedata, cmap='jet')
        else:
            plt.imsave(overlay_path, imagedata, cmap=cm)
        plt.close()
        print(f"[+] Succesfully created temp overlay heatmap '{overlay_path}")

        overlay = Image.open(overlay_path)
        overlay = overlay.convert("RGBA")

        new_img = Image.blend(background, overlay, float(
            config["HEATMAP"]["overlay"]))
        new_img.save(heatmap_path, "PNG")

        # Scatter points
        coords = [(int(i["x"]), int(i["y"])) for i in data_obj["labeldata"]]
        labels = [i["label"] for i in data_obj["labeldata"]]

        plot_points(coords, labels, heatmap_path, heatmap_path)
        print(f"[+] Succesfully created final heatmap '{heatmap_path}")


if __name__ == "__main__":
    main(DATA, CONFIG)
    save_plot_figures(DATA, CONFIG)
    save_heatmap_images(DATA, CONFIG)
