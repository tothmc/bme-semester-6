# Infokommunikációs labor 1 : 04 - WiFi hálózat mérés

## Tool for merging data .csv files into .xlsx

## 0. Requirements

### a. Clone this repository and cd into it

```bash
git clone [repo_url]
cd 04_wifi
```

### b. Generate data

Use [Wifi Heat Map - Survey](https://play.google.com/store/apps/details?id=info.wifianalyzer.heatmap) for generating the .csv data files.

The content of .csv files should look like this:

```csv
quality;x;y
-79;381.40536;497.21835
-79;402.48087;424.78406
-79;417.41788;365.44525
...
```

### c. Download data .csv files into 'src/data' directory

You should have a file structure like this:

```bash
|-- README.md
|-- config.ini
|-- main.py
|-- output
|   |-- fig
|   `-- heatmap
|-- requirements.txt
`-- src
    |-- data
    |-- img
    `-- overlay
```

### d. Change the background image

!!! You have to change the [DATA_DIR]/[IMG_DIR]/[map_filename] background image to the file that was used in the Wifi Heat Map - Survey application to avoid OutOfIndex errors. !!!

## 1. Installation

#### a. Creating a virtual environment

##### On Linux:

```bash
...
python -m venv [directory_of_virtual_environment eg.: venv]
source [directory_of_virtual_environment]/bin/activate
...
```

##### On Windows with Powershell:

```powershell
...
python -m venv [directory_of_virtual_environment eg.: venv]
.\[directory_of_virtual_environment]\Scripts\activate
...
```

##### On Windows with CMD:

```cmd
...
python -m venv [directory_of_virtual_environment eg.: venv]
.\[directory_of_virtual_environment]\Scripts\activate.bat
...
```

#### b. Installation of dependencies

```bash
...
pip install pip -r requirements.txt
...
```

### 2. Use

The program will create a unified/merged [OUTPUT_FILENAME] xlsx with the headers(SSID, MAC-address) and measured data row by row in the [OUTPUT_DIR] directory.
[OUTPUT_FILENAME], [OUTPUT_DIR] etc. are variables defined in 'config.ini'. For that you must have aboved mentioned proper .csv files in the [DATA_DIR].

If you want to generate heatmaps or plot figures change '[save_plot_figures]' '[save_heatmaps]' according your will.

```editorconfig
[PROGRAM]
save_plot_figures = true
save_heatmaps = true
```

For generating heatmaps and plot figures a valid [DATA_DIR]/[IMG_DIR]/[map_filename] image file is required.

You can customize the heatmaps in the 'config.ini':

```editorconfig
[HEATMAP]
type = cm           # Color mapping
colorbar = True     # colorbar on plot figures
sigma = 40          # Size of the effect of a data point
overlay = 0.6       # [0-1] Determines the overlay effect of the heatmap projected on the background map file.
```

Run the script by:

####

```bash
...
python main.py
...
```

### 3. After use

#### Exit from the virtual environment

##### On Linux:

```bash
...
deactivate
...
```

##### On Windows with Powershell:

```powershell
...
.\[directory_of_virtual_environment]\Scripts\deactivate
...
```

##### On Windows with CMD:

```cmd
...
.\[directory_of_virtual_environment]\Scripts\deactivate.bat
...
```

### 4. Demo - Examples

#### Content of 'src/data' directory

![Content of 'src/data' directory](demo_example/demo_content_of_src_data_dir.png "Content of 'src/data' directory")

#### Content of the generated xlsx file

![Content of the generated xlsx file](demo_example/demo_content_of_output_xlsx_file.png "Content of the generated xlsx file")

#### Content of 'output/heatmap' directory

![Content of 'output/heatmap' directory](demo_example/demo_content_of_output_heatmap_dir.png "Content of 'output/heatmap' directory")

#### Example of a generated heatmap image

![Example of a generated heatmap image](demo_example/demo_output_heatmap_file.png "Example of a generated heatmap image")
